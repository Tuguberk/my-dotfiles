
map qq :q!<Enter>
map qw :wq<Enter>


map <C-c> :NERDTreeTabsToggle<CR>
map <C-g> :Goyo<CR>
map <C-d> :Files<CR>

imap ^[h <Left>
imap ^[j <Down>
imap ^[k <Up>
imap ^[l <Right>
"imap ^[  <Esc>

imap <C-k> <Plug>(coc-snippets-expand-jump)
nmap <leader>gd <Plug>(coc-definition)
nmap <leader>gy <Plug>(coc-type-definition)
nmap <leader>gi <Plug>(coc-implementation)
nmap <leader>gr <Plug>(coc-references)
nmap <leader>rr <Plug>(coc-rename)


nmap ı <insert>



nmap , <leader>c<space>
nnoremap <F8> :!clear;g++ % -o %< 
