
map qq :q!<Enter>
map qw :wq<Enter>

map <ESC>[1;5C <C-Right>
map <ESC>[1;5D <C-Left>
map <ESC>[1;5A <C-Up>
map <ESC>[1;5B <C-Down>

map <C-c> :NERDTreeTabsToggle<CR>
nmap <F8> :Vista!!<CR>

map <C-g> :Goyo<CR>

map <C-h> <C-w>h
map <C-Left> <C-w>h
map <C-l> <C-w>l
map <C-Right> <C-w>l
map <C-j> <C-w>j
map <C-Down> <C-w>j
map <C-k> <C-w>k
map <C-Up> <C-w>k

map <C-s><up> :tabr<cr>
map <C-s><down> :tabl<cr>
map <C-s><left> :tabp<cr>
map <C-s><right> :tabn<cr>

imap <C-k> <Plug>(coc-snippets-expand-jump)
