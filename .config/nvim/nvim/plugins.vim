
if ! filereadable(expand('~/.config/nvim/autoload/plug.vim'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ~/.config/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ~/.config/nvim/autoload/plug.vim
    autocmd VimEnter * PlugInstall
endif

call plug#begin('~/.config/nvim/plugged')
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeTabsToggle' }
Plug 'Xuyuanp/nerdtree-git-plugin', { 'on': 'NERDTreeTabsToggle' }
Plug 'jistr/vim-nerdtree-tabs', { 'on': 'NERDTreeTabsToggle' }
Plug 'ryanoasis/vim-devicons', { 'on': 'NERDTreeTabsToggle' }

Plug 'liuchengxu/vista.vim'
Plug 'junegunn/goyo.vim', { 'on': 'Goyo' }
Plug 'itchyny/lightline.vim'
Plug 'maximbaz/lightline-ale'

Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
Plug 'junegunn/gv.vim'

Plug 'yggdroot/indentline'

Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdcommenter'
Plug 'mattn/emmet-vim'

Plug 'fatih/vim-go', { 'do': ':GoInstallBinaries', 'for': 'go' }
Plug 'vim-python/python-syntax', { 'for': 'python' }
Plug 'plasticboy/vim-markdown', { 'for': ['markdown', 'md'] }
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app & yarn install', 'for': ['markdown', 'md']  }
Plug 'dense-analysis/ale'
Plug 'dsawardekar/ember.vim', { 'for': ['javascript', 'handlebars.ember' ] }
Plug 'joukevandermaas/vim-ember-hbs', { 'for': ['handlebars.ember'] }

Plug 'neoclide/coc.nvim', {'branch': 'release', 'do': 'sh ~/.config/nvim/cocinstaller.sh'}

Plug 'raimondi/delimitmate'
Plug 'honza/vim-snippets'

Plug 'dracula/vim', { 'as': 'dracula' }

call plug#end()


function! SetupCommandAbbrs(from, to)
  exec 'cnoreabbrev <expr> '.a:from
        \ .' ((getcmdtype() ==# ":" && getcmdline() ==# "'.a:from.'")'
        \ .'? ("'.a:to.'") : ("'.a:from.'"))'
endfunction

" Use C to open coc config
call SetupCommandAbbrs('C', 'CocConfig')

let g:indentLine_color_term = 239
let g:indentLine_char_list = ['|', '¦', '┆', '┊']

let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_conceal = 0
let g:vim_markdown_conceal_code_blocks = 0

let g:goyo_width = 100

let g:user_emmet_mode = 'a'
let g:user_emmet_install_global = 0
autocmd FileType html,css,htmldjango EmmetInstall

let g:delimitMate_autoclose = 1
let g:delimitMate_matchpairs = "(:),[:],{:},<:>"
let g:delimitMate_jump_expansion = 1
let g:delimitMate_expand_space = 1
let g:delimitMate_expand_cr = 2
let g:delimitMate_expand_inside_quotes = 1
autocmd FileType markdown let g:delimitMate_expand_space = 0

let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'

let g:ale_lint_on_text_changed = 'never'
let g:ale_lint_on_insert_leave = 0
let g:ale_open_list = 1
let g:ale_list_window_size = 3
highlight ALEError ctermbg=White
highlight ALEError ctermfg=DarkRed
highlight ALEWarning ctermbg=LightYellow
highlight ALEWarning ctermfg=Darkmagenta

autocmd FileType python let b:ale_linters = ['flake8']
autocmd FileType python let b:ale_fixers = ['autopep8', 'yapf']
autocmd FileType python let b:ale_warn_about_trailing_whitespace = 0
autocmd FileType json let g:indentLine_conceallevel = 0

" python syntax
let g:python_highlight_all = 1
