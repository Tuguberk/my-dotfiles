#!/bin/sh
nvim -c 'CocInstall -sync coc-json coc-git coc-explorer coc-highlight \
    coc-marketplace coc-snippets coc-html coc-xml coc-vimlsp coc-vetur|q'
nvim -c 'coc-tsserver coc-python coc-go coc-ember coc-css coc-clangd coc-template \
    coc-flutter coc-sh coc-docker coc-lua coc-yaml|q'
