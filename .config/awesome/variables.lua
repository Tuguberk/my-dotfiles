local awful = require("awful")
local utils = require("utils")

terminal = "alacritty"
browser1 = "brave"
browser2 = "qutebrowser"
editor = os.getenv("EDITOR") or "nvim"
editor_cmd = terminal .. " -e " .. editor
file1 = terminal .. " -e " .. "ranger"
file2 = "nemo"


thermal_zone = 0

lockscreen = "betterlockscreen -l -t 'Get out of here!'"

awful.layout.layouts = {
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.corner.nw,
    utils.centerwork,
    awful.layout.suit.floating,
}

awful.util.tagnames = {
	{
		{name = "1", sgap = true},
		{name = "2", sgap = true},
		{name = "3", sgap = true},
		{name = "4", sgap = true},
		{name = "5", sgap = true},
		{name = "6", sgap = true},
		{name = "7", sgap = true},
		{name = "8", sgap = true},
		{name = "9", sgap = true, sel = true},
		{name = "10",sgap = true},
	},

	{
		{name = "1", sgap = true ,sel = true},
		{name = "2", sgap = true},
		{name = "3", sgap = true},
		{name = "4", sgap = true},
		{name = "5", sgap = true},
		{name = "6", sgap = true},
		{name = "7", sgap = true},
		{name = "8", sgap = true},
		{name = "9", sgap = true},
		{name = "10",sgap = true},
	}
}
